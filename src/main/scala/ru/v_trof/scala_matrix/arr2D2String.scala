package ru.v_trof.scala_matrix

object arr2D2String {
  def d2(m: Array[Array[Double]]): String = {
    m.foldLeft("")((s, row) => s + row.foldLeft("")((s, el) => s + f"$el%.8g\t") + "\n")
  }

  def d2FirstLast(m: Array[Array[Double]]): String = {
    f"${m.head.head}%.8f\t ${m.last.last}%.8f\n"
  }
}
