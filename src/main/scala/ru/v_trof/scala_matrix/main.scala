package ru.v_trof.scala_matrix

object main {
  def main(args: Array[String]): Unit = {
    val defaultTargetTime = 1
    val defaultAccuracy = 0.1
    val defaultEnsureAttempts = 10

    if (args.length < 2) {
      print(
        s"Usage java -jar this-program.jar {matrix1Base} {matrix2Base} " +
          s"[optionalTargetTime=$defaultTargetTime)" +
          s"[timeAccuracy=$defaultAccuracy)" +
          s"[ensureAttempts=$defaultEnsureAttempts)"
      )
      return
    }

    val runner = new MatrixRunner(
      args(0).toDouble, args(1).toDouble,
      if (args.length > 4 && !args(4).isEmpty) args(4).toInt else defaultEnsureAttempts
    )

    val res = runner.findSizeForTime(
      targetTime = if (args.length > 2 && !args(2).isEmpty) args(2).toDouble else defaultTargetTime,
      accuracy = if (args.length > 3 && !args(3).isEmpty) args(3).toDouble else defaultAccuracy,
      log = true
    )

    println(s"size ${res._1}")
    println(s"mean ${res._2._1}")
    println(s"diff ${res._2._2}")

//        print(arr2D2String.d2FirstLast(runner.run(runner.prepare(args(2).toInt)).values))
  }
}