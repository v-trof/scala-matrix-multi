package ru.v_trof.scala_matrix

class Matrix(val values: Array[Array[Double]]) {
  val cols: Int = values.head.length
  val rows: Int = values.length

  def multiply(multiple: Matrix): Matrix = {
    if (cols != multiple.rows) throw new Error("Invalid dimensions for Matrix multiplication")

    val resValues = Array.ofDim[Double](rows, cols)

    for (row <- 0 until rows; col <- 0 until multiple.cols) {
      var sum = 0.0
      for (mapper <- 0 until cols) {
        sum += this.values(row)(mapper) * multiple.values(mapper)(col)
      }
      resValues(row)(col) = sum
    }

    new Matrix(resValues)
  }

  def *(multiple: Matrix): Matrix = multiply(multiple)

  override def toString: String = s"Matrix($rows, $cols)\n" + arr2D2String.d2(values)
}
