package ru.v_trof.scala_matrix

// WHY THE FUCK NO GLOBAL TYPE ALIASES

class MatrixRunner(base1: Double, base2: Double, ensureAttempts:Int = 10)
  extends TimeFinder[Int, (Matrix, Matrix), Matrix](ensureAttempts = ensureAttempts) {

  override def prepare(size: Int): (Matrix, Matrix) = (
      new Matrix(util.generateInitialValues(size, base1, 'a')),
      new Matrix(util.generateInitialValues(size, base2, 'b'))
  )


  override def run(ms: (Matrix, Matrix)): Matrix = ms._1 * ms._2

  override def res2string(result: Matrix): String = arr2D2String.d2FirstLast(result.values)

  override def nextPrepareArgs(attempts: Array[Attempt[Int]], targetTime: Double): Int = {
    if(attempts.length == 0) return 100

    val suggestions = attempts.map(x => scala.math.cbrt(targetTime/x.time) * x.args)
    val nextSize = suggestions.sum / attempts.length

    if(nextSize > 0) nextSize.toInt else 1
  }
}
