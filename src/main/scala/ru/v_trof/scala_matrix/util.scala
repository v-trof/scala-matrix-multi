package ru.v_trof.scala_matrix

object util {
  val MUL: Double = Math.pow(10, -7)

  def generateInitialValues(size: Int, base: Double, matrix: Char): Array[Array[Double]] = {
    def elA(x: Int, y: Int) = base + MUL * (x + size * y)
    def elB(x: Int, y: Int) = base - MUL * (x + size * y)

    def row(y: Int) = (0 until size).map(x => if(matrix == 'a') elA(x, y) else elB(x, y)).toArray

    (0 until size).map(row).toArray
  }

  def execTime[R](block: => R): (Double, R) = {
    val start = System nanoTime()
    val result = block
    val end = System nanoTime()
    val elapsed = (end - start) * Math.pow(10, -9) // ns to s
    (elapsed, result)
  }
}
