package ru.v_trof.scala_matrix
import scala.util.control.Breaks._

case class Attempt[Args](args: Args, time: Double)

abstract class TimeFinder
  [PrepareArgs, RunArgs, RunResult]
  (ensureAttempts: Int = 10)
{

  def prepare(args: PrepareArgs): RunArgs
  def run(args: RunArgs): RunResult
  def nextPrepareArgs(attempts: Array[Attempt[PrepareArgs]], targetTime: Double): PrepareArgs
  def res2string(res: RunResult): String

  def measure(args: PrepareArgs): (Double, RunResult) = {
    val runArgs = prepare(args)
    util.execTime(run(runArgs))
  }

  def ensure(args: PrepareArgs, log: Boolean = false): (Double, Double) = {
    val times =(0 until ensureAttempts).map({ _ =>
      val lastTime = measure(args)._1
      if (log) println(f"Ensured $args to compute $lastTime%.6f seconds")

      lastTime
    })

    val mean = times.sum / times.length
    val diff = times.foldLeft(0.0)((acc, x) => acc + Math.abs(x-mean)) / times.length
    val maxDiff = math.max(math.abs(times.min - mean), math.abs(times.max - mean))
    val diffPercent = maxDiff * 100

    if (log) {
      println(f"Mean Time\t$mean%.6f")
      println(f"Diff MaxVal\t$maxDiff%.6f")
      println(f"Diff Max %%\t$diffPercent%.6f")
    }

    (mean, diff)
  }

  def findSizeForTime(targetTime: Double, accuracy: Double, log: Boolean = false): (PrepareArgs, (Double, Double)) = {
    var attempts = new Array[Attempt[PrepareArgs]](0)

    breakable {
      do {
        val nextArgs = nextPrepareArgs(attempts, targetTime)
        if (attempts.length > 3 && nextArgs == attempts.last.args) break

        if (log) println(s"Attempting $nextArgs...")
        val (time, res) = measure(nextArgs)
        attempts :+= new Attempt[PrepareArgs](nextArgs, time)

        if (log) println(f"${attempts.last.args} took ${attempts.last.time}%.6f seconds to compute ${res2string(res)}")
      } while (Math.abs(attempts.last.time - targetTime) > accuracy)
    }

    val mean = ensure(attempts.last.args, log)

    if (log) println("Done")
    (attempts.last.args, mean)
  }
}
